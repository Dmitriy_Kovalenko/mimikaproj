using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;
using FaceMaskExample;

public class BasicAgent : Agent
{
    [Header("Specific to Basic")]
    private BasicAcademy academy;
    public WebCamTextureFaceMaskExample face_tracker;
    public EmojiDetector emojiDetector;
    public float[] landmarks_observations;
    public KeyCode correct_key = KeyCode.Alpha1;

    public override void InitializeAgent()
    {
        academy = FindObjectOfType(typeof(BasicAcademy)) as BasicAcademy;
        landmarks_observations = new float[136];
    }

    public override void CollectObservations()
    {
        //Debug.Log("CollectObservations " + gameObject.name + " send " + face_tracker.faceLandmarkPointsInMask.Count);
        if (face_tracker.correctedFacepoints.Count != 68)
            return;

        for (int i=0; i< 68; i++)
        {
            landmarks_observations[i * 2] = face_tracker.correctedFacepoints[i].x;
            landmarks_observations[i * 2 + 1] = face_tracker.correctedFacepoints[i].y;
        }

        AddVectorObs(landmarks_observations);
    }

    void updateEmojiState(bool detected)
    {
        int index = (int)correct_key - 49;
        emojiDetector.detectedEmoji[index] = detected;
    }

    public override void AgentAction(float[] vectorAction, string textAction)
	{
        var choice = (int)vectorAction[0];

        updateEmojiState(choice == 1);

        if (Input.GetKeyUp(KeyCode.Alpha1) ||
            Input.GetKeyUp(KeyCode.Alpha2) ||
            Input.GetKeyUp(KeyCode.Alpha3) ||
            Input.GetKeyUp(KeyCode.Alpha4) ||
            Input.GetKeyUp(KeyCode.Alpha5))
        {
            if (Input.GetKeyUp(correct_key) && choice == 1)
            {
                Debug.Log("AgentAction " + gameObject.name +" got "+choice + " reward 1.0");
                AddReward(1.0f);
            }
            else
            {
                Debug.Log("AgentAction " + gameObject.name + " got " + choice + " reward -0.1");
                AddReward(-0.1f);
            }
        } else if (Input.GetKeyUp(KeyCode.Alpha0))
        {
            AddReward(0.0f);
        }
    }

    public override void AgentReset()
    {
        
    }

    public override void AgentOnDone()
    {

    }

    public void FixedUpdate()
    {
        WaitTimeInference();
    }

    private void WaitTimeInference()
    {
        if (!academy.GetIsInference())
        {
            if (Input.GetKeyUp(KeyCode.Alpha1) ||
            Input.GetKeyUp(KeyCode.Alpha2) ||
            Input.GetKeyUp(KeyCode.Alpha3) ||
            Input.GetKeyUp(KeyCode.Alpha4) ||
            Input.GetKeyUp(KeyCode.Alpha5) ||
            Input.GetKeyUp(KeyCode.Alpha0))
            {
                RequestDecision();
            }
        }
    }

}
