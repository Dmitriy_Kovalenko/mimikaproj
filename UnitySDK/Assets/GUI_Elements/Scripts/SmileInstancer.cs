﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmileInstancer : MonoBehaviour {

    public GameObject Enemy;
    public Material[] EnemyMats;

    private int timer;

    [Range(100, 300)]
    public int EnemySeedTime;

    private Vector3 _position;
    private Quaternion _rotation;

    private void Start()
    {
        _position = Vector3.zero;
        _rotation = Quaternion.identity;
    }

    void Update ()
    {
        timer++;
        if (timer > EnemySeedTime)
        {
            timer = 0;
            StartCoroutine(InstanceEnemy());
        }
	}

    IEnumerator InstanceEnemy()
    {
        yield return new WaitForSeconds(Mathf.Clamp(Random.value, 0.1f, 0.5f));

        GameObject GO = Instantiate(Enemy, _position, _rotation);

        int RandomInt = (int)Random.Range(0.0f, (float)EnemyMats.Length);

        GO.GetComponent<Renderer>().material = EnemyMats[RandomInt];
        GO.GetComponent<SmileDriver>()._emotion = (SmileDriver.Emotion)RandomInt;
    }
}
