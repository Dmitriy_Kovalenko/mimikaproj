﻿using UnityEngine;

public class UI_TextDriver : MonoBehaviour {

    public int _timer;
    private bool _stop;
	
	void Update () {

        if (!_stop) _timer--;

        if (_timer < 0)
        {
            _stop = true;
            Destroy(gameObject);
        }
	}
}
