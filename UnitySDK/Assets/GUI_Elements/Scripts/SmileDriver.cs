﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmileDriver : MonoBehaviour {

    [System.Serializable]
    public enum Emotion
    {
        Angry,
        Confused,
        Happy,
        Sad,
        Surprised,
        Empty
    }

    [System.Serializable]
    public enum Direction
    {
        Left,
        Right
    }

    [System.Serializable]
    public enum UpPosition
    {
        One,
        Two,
        Three,
        Four
    }

    public Emotion _emotion;
    public Direction _direction;
    public UpPosition _upPosition;

    public float speed;
    public float DeathTime;

    public AnimationCurve myCurve;
    public float ScaleCurve;

    private void Start()
    {
        _upPosition = (UpPosition)(int)Mathf.Clamp(Random.Range(0, 4), 0, 3);
        _direction = (Direction)(int) Mathf.Clamp(Random.Range(0, 2), 0, 1);

        float X_float = 0;
        float Y_float = 0;

        if (_upPosition == UpPosition.One) Y_float = 1.68f;
        if (_upPosition == UpPosition.Two) Y_float = 1.325f;
        if (_upPosition == UpPosition.Three) Y_float = 0.975f;
        if (_upPosition == UpPosition.Four) Y_float = 0.62f;

        if (_direction == Direction.Left) X_float = -1.0f;
        if (_direction == Direction.Right) X_float = 1.0f;

        // transform.localScale = Vector3.one * val;

        transform.position = new Vector3(X_float, Y_float, -0.05f);
    }

    void Update ()
    {
        DeathTime--;

        ScaleCurve += 0.005f;

        float val = myCurve.Evaluate(ScaleCurve);

        if (_direction == Direction.Left) transform.position = new Vector3 (transform.position.x + 0.001f * speed * val, transform.position.y, transform.position.z);
        if (_direction == Direction.Right) transform.position = new Vector3(transform.position.x - 0.001f * speed * val, transform.position.y, transform.position.z);

        if (DeathTime < 0) Destroy(gameObject);
    }
}
