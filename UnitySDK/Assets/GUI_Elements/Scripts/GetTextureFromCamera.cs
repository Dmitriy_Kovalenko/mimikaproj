﻿using UnityEngine;
using System.Collections;

public class GetTextureFromCamera : MonoBehaviour {

    public float Scale = 1.0f;
    WebCamTexture webcamTexture;


    void Start ()
    {
        webcamTexture = new WebCamTexture();
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = webcamTexture;
        webcamTexture.Play();
        StartCoroutine(SetCameraFrameSize());
    }

    IEnumerator SetCameraFrameSize()
    {
        yield return new WaitForSeconds(0.05f);
        transform.localScale = new Vector3(webcamTexture.width * 1.0f / webcamTexture.height, 1, 1) * Scale;
    }
}
