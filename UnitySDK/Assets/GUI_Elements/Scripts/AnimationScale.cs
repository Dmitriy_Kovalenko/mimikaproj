﻿using UnityEngine;

public class AnimationScale : MonoBehaviour
{
    public AnimationCurve myCurve;

    private float ModifiedScale;
    
    public float CurrentScale;
    public float RangeScale;
    public float SpeedScale;
    public bool _pause;
    public bool _autopause;

    private bool reverse;

    private void Start()
    {
        _pause = true;
        ModifiedScale = CurrentScale;
    }

    void Update()
    {
        if (!_pause)
        {
            if (ModifiedScale > CurrentScale + RangeScale)
            {
                reverse = true;
            }

            if (ModifiedScale <= CurrentScale - RangeScale)
            {
                reverse = false;
                if (_autopause) _pause = true;
            }

            if (!reverse) ModifiedScale += 0.001f * SpeedScale;
            else ModifiedScale -= 0.001f * SpeedScale;

            float val = myCurve.Evaluate(ModifiedScale);
            transform.localScale = Vector3.one * val;
        }
    }
}
