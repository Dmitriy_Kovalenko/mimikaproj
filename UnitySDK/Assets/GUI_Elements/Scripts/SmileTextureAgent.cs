﻿using UnityEngine;

public class SmileTextureAgent : MonoBehaviour {

    public Texture[] SmileTexture;
    public Texture BlankTexture;

    private void Start()
    {
        SetBlankTexture();
    }

    public void SetBlankTexture()
    {
        GetComponent<Renderer>().material.mainTexture = BlankTexture;
    }

    public void SetSmileTexture (int numTexure)
    {
        if (numTexure <= SmileTexture.Length - 1)
            GetComponent<Renderer>().material.mainTexture = SmileTexture[numTexure];
        else Debug.Log("Wrong texute number");
    }
}

